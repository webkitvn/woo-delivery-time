(function ($) {
    if ($('body').hasClass('single-product')) {
        tippy('[data-tippy-content]', {
            theme: 'light',
        });
    }
})(jQuery);